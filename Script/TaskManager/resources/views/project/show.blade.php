@extends('layouts.app')

@section('content')
<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Project</h1>
          <p>Daftar Project</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/home">Home</a></li>
        </ul>
      </div>
<h4>{{ $project->title }}</h4>
<p>{{ $project->content }}</p>
<a href="{{ route('project.index') }}" class="btn btn-default">Kembali</a>
</main>
@endsection