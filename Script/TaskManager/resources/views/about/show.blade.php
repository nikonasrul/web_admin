@extends('layouts.app')

@section('content')
<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> About</h1>
          <p>Tentang Kami</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/home">Home</a></li>
        </ul>
      </div>
<h4>{{ $about->title }}</h4>
<p>{{ $about->content }}</p>
<a href="{{ route('about.index') }}" class="btn btn-default">Kembali</a>
</main>
@endsection