<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/article', 'ArtikelController@index');
Route::get('/project', 'ProjectController@index');
Route::get('/about', 'AboutController@index');
Route::get('/faq', 'FaqController@index');
Route::get('/contact', 'ContactController@show');
Route::post('/contact',  'ContactController@mailToAdmin');

Route::group(['prefix' => 'social-media', 'namespace' => 'Auth'], function(){
    Route::get('register/{provider}', 'SocialiteController@register');
    Route::get('registered/{provider}', 'SocialiteController@registered');

Route::get('/', function () {
return view('welcome');
});
Route::resource('crud', 'CRUDController');
});
Route::resource('article', 'ArticleController');
Route::resource('project', 'ProjectController');
Route::resource('about', 'AboutController');
Route::resource('faq', 'FaqController');