@extends('layouts.app')

@section('content')
<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> Artikel</h1>
          <p>Berita Terbaru</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/home">Home</a></li>
        </ul>
      </div>
<h4>{{ $article->title }}</h4>
<p>{{ $article->content }}</p>
<a href="{{ route('article.index') }}" class="btn btn-default">Kembali</a>
</main>
@endsection